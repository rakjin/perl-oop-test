#!/usr/bin/env perl
use strict;
use warnings;

use Acme::Animal;

my $animal = Acme::Animal->new( name => 'Jake' );
print '$animal->{legs}: ', $animal->{legs}, "\n";
print '$animal->{name}: ', $animal->{name}, "\n";

$animal->bark();
print "\n";
$animal->bark(3);
