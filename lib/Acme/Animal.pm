package Acme::Animal;
use strict;
use warnings;

sub new {
    my $class = shift;
    my %args = @_;
    my $self = {
        legs => 4,
        color => 'brown',
        name => $args{name},
    };
    return bless $self, $class;
}

sub bark {
    my ($self, $times) = @_;
    $times ||= 1;

    for (1..$times) {
        print 'My Name is ', $self->{name}, "!\n";
    }
}


1;
